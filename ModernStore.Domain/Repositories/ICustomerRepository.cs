﻿using ModernStore.Domain.Entities;
using ModernStore.Shared.Commands;
using System;

namespace ModernStore.Domain.Repositories
{
    public interface ICustomerRepository
    {
        Customer Get(Guid id);
        Customer GetByUserId(Guid id);
        void Save(Customer customer);
    }
}
