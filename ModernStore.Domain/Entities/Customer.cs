﻿using FluentValidator;
using ModernStore.Domain.ValueObjects;
using ModernStore.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModernStore.Domain.Entities
{
    public class Customer : Entity
    {
        protected Customer()
        {
        }
        public Customer(Name name,
            DateTime birthDate,
            User user,
            Email email,
            Document document)
        {
            Name = name;
            BirthDate = birthDate;
            User = user;
            Email = email;
            Document = document;

            AddNotifications(Name.Notifications);
            AddNotifications(Email.Notifications);
            AddNotifications(Document.Notifications);

        }
        
        public Name Name { get; private set; }
        public DateTime BirthDate { get; private set; }
        public User User { get; private set; }
        public Email Email { get; private set; }
        public Document Document { get; private set; }
    }
}
