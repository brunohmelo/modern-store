﻿using ModernStore.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModernStore.Domain.Entities
{
    public class User : Entity
    {
        protected User()
        {
        }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public bool Active { get; private set; }

        public User(string username, string password)
        {
            Username = username;
            Password = password;
            Active = false;
        }

        public void Activate()
        {
            //TODO - Validate
            Active = true;
        }

    }
}
