﻿using FluentValidator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModernStore.Domain.ValueObjects
{
    public class Name : Notifiable
    {
        public Name(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;

            new ValidationContract<Name>(this)
                .IsRequired(x => x.FirstName)
                .HasMaxLenght(x => x.FirstName, 3)
                .HasMaxLenght(x => x.FirstName, 250)
                .IsRequired(x => x.LastName)
                .HasMaxLenght(x => x.LastName, 3)
                .HasMaxLenght(x => x.LastName, 250);

            if (FirstName.Length < 3)
            {
                AddNotification("Name", "Nome Inválido");
            }

            if (LastName.Length < 3)
            {
                AddNotification("LastName", "Sobrenome inválido.");
            }
        }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
    }
}
