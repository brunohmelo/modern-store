﻿using ModernStore.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModernStore.Domain.CommandResults
{
    public class RegisterCustomerCommandResults : ICommandResult
    {
        public RegisterCustomerCommandResults()
        {

        }
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
