﻿using FluentValidator;
using ModernStore.Domain.CommandResults;
using ModernStore.Domain.Commands;
using ModernStore.Domain.Entities;
using ModernStore.Domain.Repositories;
using ModernStore.Domain.Services;
using ModernStore.Domain.ValueObjects;
using ModernStore.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModernStore.Domain.CommandHandler
{
    public class CustomerCommandHandler : Notifiable,
        ICommandHandler<RegisterCustomerCommand>
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IEmailService _emailService;
        public CustomerCommandHandler(ICustomerRepository customerRepository, IEmailService emailService)
        {
            _customerRepository = customerRepository;
        }

        public ICommandResult Handle(RegisterCustomerCommand command)
        {
            var name = new Name("Bruno", "Melo");
            var nascimento = new DateTime(1992, 04, 15);
            var email = new Email("contato@brunohmelo.com");
            var user = new User("brunohmelo", "HS!¨*@#!¨HD");
            var document = new Document("07535551904");
            var customer = new Customer(name, nascimento, user, email, document);

            AddNotifications(customer.Notifications);

            if (IsValid())
                _customerRepository.Save(customer);

            _emailService.Send(name.ToString(), email.ToString(), "Bem-Vindo ao Clube!", $"<h1>Olá, <strong>{customer.Name.ToString()}</strong> ,seja bem vindo!</h1>");

            return new RegisterCustomerCommandResults()
            {
                Id = customer.Id,
                Name = customer.Name.ToString()
            };
        }
    }
}
