﻿using ModernStore.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ModernStore.Infra.Map
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            ToTable("Products");
            HasKey(x => x.Id);
            Property(x => x.Title).IsRequired().HasMaxLength(60);
            Property(x => x.Price).IsRequired().HasColumnType("money");
            Property(x => x.QuantityOnHand);
            Property(x => x.Image).HasMaxLength(1024);
        }
    }
}
