﻿using ModernStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModernStore.Infra.Map
{
    public class OrderItemsMap : EntityTypeConfiguration<OrderItem>
    {
        public OrderItemsMap()
        {
            ToTable("OrderItems");
            HasKey(x => x.Id);
            Property(x => x.Price).IsRequired().HasColumnType("money");
            Property(x => x.Quantity).IsRequired();
            HasRequired(x => x.Product);
        }
    }
}
