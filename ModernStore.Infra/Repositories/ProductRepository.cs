﻿using ModernStore.Domain.Repositories;
using System;
using System.Linq;
using ModernStore.Domain.Entities;
using ModernStore.Infra.Contexts;

namespace ModernStore.Infra.Repositories
{
    public class ProductRepository: IProductRepository
    {
        private readonly ModernStoreContexts _context;
        public ProductRepository(ModernStoreContexts context)
        {
            _context = context;
        }

        public Product Get(Guid id)
        {
            return _context.Products.AsNoTracking().FirstOrDefault(x => x.Id == id);
        }
    }
}
