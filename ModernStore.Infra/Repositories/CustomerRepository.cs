﻿using ModernStore.Domain.Repositories;
using System;
using System.Linq;
using System.Data.Entity;
using ModernStore.Domain.Entities;
using ModernStore.Infra.Contexts;

namespace ModernStore.Infra.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly ModernStoreContexts _context;
        public CustomerRepository(ModernStoreContexts context)
        {
            _context = context;
        }
        public Customer Get(Guid id)
        {
            return _context.Customers
                    .Include(x => x.User)
                    .FirstOrDefault(x => x.Id == id);
        }

        public Customer GetByUserId(Guid id)
        {
            return _context.Customers
                    .Include(x => x.User)
                    .FirstOrDefault(x => x.User.Id == id);
        }

        public void Save(Customer customer)
        {
            _context.Customers.Add(customer);
        }
    }
}
