﻿using ModernStore.Domain.Entities;
using ModernStore.Infra.Map;
using System.Data.Entity;

namespace ModernStore.Infra.Contexts
{
    public class ModernStoreContexts : DbContext
    {
        public ModernStoreContexts():base("Server=PC-BRUNO\\SQLEXPRESS;Database=ModernStore;Trusted_Connection=True")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CustomerMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new ProductMap());
            modelBuilder.Configurations.Add(new OrderMap());
            modelBuilder.Configurations.Add(new OrderItemsMap());
        }
    }
}
