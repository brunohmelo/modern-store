﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModernStore.Domain.Entities;
using FluentValidator;
using ModernStore.Domain.ValueObjects;

namespace ModernStore.Domain.Tests
{

    [TestClass]
    public class CustomerTests
    {
        private readonly User user = new User("brunohmelo", "12345");

        [TestMethod]
        [TestCategory("Customer - New Customer")]
        public void GivenAnInvalidFirstNameShouldReturnANotification()
        {
            var customer = new Customer(new Name("", "Melo"), new DateTime(1992, 04, 15), user, new Email("brunohmelo@brunohmelo.com"), new Document("07535551904"));
            Assert.IsFalse(customer.IsValid());
        }

        [TestMethod]
        [TestCategory("Customer - New Customer")]
        public void GivenAnInvalidLastNameShouldReturnANotification()
        {
            var customer = new Customer(new Name("Bruno", ""), new DateTime(1992, 04, 15), user, new Email("brunohmelo@brunohmelo.com"), new Document("07535551904"));
            Assert.IsFalse(customer.IsValid());
        }

        [TestMethod]
        [TestCategory("Customer - New Customer")]
        public void GivenAnInvalidEmailShouldReturnANotification()
        {
            var customer = new Customer(new Name("Bruno", "Melo"), new DateTime(1992, 04, 15), user, new Email(""), new Document("07535551904"));
            Assert.IsFalse(customer.IsValid());
        }
    }
}
