﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace ModernStore.Api.Controllers
{
    public class ProductController : Controller
    {
        [HttpGet]
        [Route("Products/{id}")]
        public string Get(Guid id)
        {
            return $"produtos {id}";
        }

        [HttpPost]
        [Route("Products")]
        public void Create()
        {

        }
    }
}
