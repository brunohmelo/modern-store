﻿using Microsoft.AspNetCore.Mvc;
using ModernStore.Domain.Commands;
using ModernStore.Domain.Repositories;
using ModernStore.Domain.CommandHandler;
using System;
using ModernStore.Domain.Services;
using ModernStore.Infra.Transactions;

namespace ModernStore.Api.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IEmailService _emailService;
        private readonly IUow _uow;
        public CustomerController(ICustomerRepository customerResitory, IEmailService emailService, IUow uow)
        {
            _customerRepository = customerResitory;
            _emailService = emailService;
            _uow = uow;
        }

        [HttpGet]
        [Route("Customers")]
        public void Create([FromBody] RegisterCustomerCommand command)
        {
            var handler = new CustomerCommandHandler(_customerRepository, _emailService);
            handler.Handle(command);

            if (handler.IsValid())
                _uow.Commit();
        }
    }
}
