﻿using ModernStore.Domain.CommandHandler;
using ModernStore.Domain.Commands;
using ModernStore.Domain.Entities;
using ModernStore.Domain.Repositories;
using ModernStore.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModernStore.Shared.Commands;

namespace ModernStore.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var command = new RegisterOrderCommand()
            {
                Customer = Guid.NewGuid(),
                DeliveryFee = 9,
                Discount = 30,
                Items = new List<RegisterOrderItemCommand>()
                {
                    new RegisterOrderItemCommand()
                    {
                        Product = Guid.NewGuid(),
                        Quantity = 3
                    }
                }
            };

            GenerateOrder(new FakeCustomerRepository(),
                new FakeProductRepository(),
                new FakeOrderRepository(),
                command);
            System.Console.ReadKey();
        }

        public static void GenerateOrder(
            ICustomerRepository customerRepository,
            IProductRepository productRepository,
            IOrderRepository orderRepository,
            RegisterOrderCommand command)
        {
            var handler = new OrderCommandHandler(customerRepository, productRepository, orderRepository);
            handler.Handle(command);
            //var customer = customerRepository.Get(command.Customer);
            //var order = new Order(customer, command.DeliveryFee, command.Discount);
            //foreach(var item in command.Items)
            //{
            //    var product = productRepository.Get(item.Product);
            //    order.AddItem(new OrderItem(product, item.Quantity));
            //}

            //var mouse = new Product("Mouse", 299, "mouse.jpg", 5);
            //var mousepad = new Product("MousePad", 80, "mousepad.jpg", 5);
            //var teclado = new Product("Teclado", 599, "teclado.jpg", 5);

            //var order = new Order(customer, 8, 10);
            //order.AddItem(new OrderItem(mouse, 2));
            //order.AddItem(new OrderItem(mousepad, 3));
            //order.AddItem(new OrderItem(teclado, 1));

            //System.Console.WriteLine($"Número do pedido: {order.Number}");
            //System.Console.WriteLine($"Cliente: {order.Customer.Name.FirstName} {order.Customer.Name.LastName}");
            //System.Console.WriteLine($"Data: {order.CreateDate:dd/MM/yyyy}");
            //System.Console.WriteLine($"Desconto: {order.Discount}");
            //System.Console.WriteLine($"Taxa de entrega: {order.DeliveryFee}");
            //System.Console.WriteLine($"Sub Total: {order.SubTotal()}");
            //System.Console.WriteLine($"Total: {order.Total() }");
        }
    }

    public class FakeCustomerRepository : ICustomerRepository
    {
        public Customer Get(Guid id)
        {
            return null;
        }

        public Customer GetByUserId(Guid id)
        {
            var name = new Name("Bruno", "Melo");
            var nascimento = new DateTime(1992, 04, 15);
            var email = new Email("contato@brunohmelo.com");
            var user = new User("brunohmelo", "HS!¨*@#!¨HD");
            var document = new Document("07535551904");
            return new Customer(name, nascimento, user, email, document);
        }

        public void Save(Customer customer)
        {
        }
    }

    public class FakeProductRepository : IProductRepository
    {
        public Product Get(Guid id)
        {
            return new Product("Mouse", 299, "mouse.jpg", 5); ;
        }
    }

    public class FakeOrderRepository : IOrderRepository
    {
        public void Save(Order order)
        {
        }
    }
}
